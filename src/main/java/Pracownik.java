import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
public class Pracownik {

    @Id
    @GeneratedValue
    private int id;
    @Column(length = 30)
    private String imie;
    private String nazwisko;
    private String departament;
    private int wynagrodzenie;

    public Pracownik(String imie, String nazwisko, String departament, int wynagrodzenie) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.departament = departament;
        this.wynagrodzenie = wynagrodzenie;

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setDepartament(String departament) {
        this.departament = departament;
    }

    public void setWynagrodzenie(int wynagrodzenie) {
        this.wynagrodzenie = wynagrodzenie;
    }

    @Override
    public String toString() {
        return "Pracownik{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", departament='" + departament + '\'' +
                ", wynagrodzenie=" + wynagrodzenie +
                '}';
    }
}
