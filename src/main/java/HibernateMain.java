import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

public class HibernateMain {
    public static void main(String[] args) {
        try {
            SessionFactory sessionFactory = new Configuration()
                    .configure("hibernate.cfg.xml")
                    .buildSessionFactory();
            EntityManager entityManger = sessionFactory.createEntityManager();
            Pracownik mariusz = new Pracownik("Mariusz", "Charusta", "TomTom", 30_000);
            Pracownik marcin = new Pracownik("Marcin", "Kowalski", "Idemia", 20_000);
            Pracownik natalia = new Pracownik("Natalia", "Guzowska", "Mercedes", 30_000);
            Pracownik marta = new Pracownik("Marta", "Kowalska", "Nokia", 40_000);

            entityManger.getTransaction().begin();
            entityManger.persist(mariusz);
            entityManger.persist(marcin);
            entityManger.persist(natalia);
            entityManger.persist(marta);
            entityManger.getTransaction().commit();

            //pobieranie obiektu za pomoca metody find i identyfikatora w tabeli
            Pracownik findPracownik = entityManger.find(Pracownik.class, 1);
            System.out.println(findPracownik);
            //podmiana wartosci wynagordzenia dla znalezionego obiektu i jego update w bazie
            findPracownik.setWynagrodzenie(20_000);
            System.out.println(findPracownik);
            entityManger.getTransaction().begin();
            entityManger.persist(findPracownik);
            entityManger.getTransaction().commit();

            //pobieranie pojedynczego obiektu za pomoca metody getSingleResult i zapytania JPQL
            Query queryResult = entityManger.createQuery("select p from Pracownik p where p.departament = 'Nokia'");
            Pracownik singleResult = (Pracownik) queryResult.getSingleResult();
            System.out.println(singleResult);
            System.out.println();

            //pobieramy wiele rekordow z tabeli za pomoca metody getResultList w wyniku czego otrzymujemy liste obiektow
            queryResult = entityManger.createQuery("select p from Pracownik p where p.wynagrodzenie >20000");
            List<Pracownik> resultList = queryResult.getResultList();
            for (Pracownik tempPracownik: resultList){
                System.out.println(tempPracownik);
            }

            entityManger.close();
            sessionFactory.close();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
}
